const filterBy = (arr, dataType) => arr.filter(item => typeof item === dataType);

const arr = ['hello', 'world', 23, '23', null];

console.log('string:', filterBy(arr, 'string'));
console.log('number:', filterBy(arr, 'number'));
console.log('object:', filterBy(arr, 'object'));